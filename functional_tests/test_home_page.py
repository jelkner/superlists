import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Natalia is trying to herd the cats that comprise the NOVA
        # Webdevelopment dev team. She hears there's a cool Django to-do app
        # which might help her.  She goes to check out its homepage.
        home_page = self.browser.get(self.live_server_url + '/')

        # She notices the page title and header mention to-do lists.
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # She is invited enter a to-do item straight away.
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # She types "Get Stefan and German to collaborate" into a text box.
        inputbox.send_keys('Get Stefan and German to collaborate')

        # When she hits enter, the page updates, and now the page lists
        # "1. Get Stefan and German to collaborate" as an item in a to-do list.
        inputbox.send_keys(Keys.ENTER)
        time.sleep(1)
        self.check_for_row_in_list_table(
            '1. Get Stefan and German to collaborate'
        )

        # There is still a text box inviting her to add another item. She
        # enters "Work with Lena and Alan on Sign-and-Read".
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Work with Lena and Alan on Sign-and-Read')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(1)

        # The page updates again, and now shows both items on her list.
        self.check_for_row_in_list_table(
            '1. Get Stefan and German to collaborate'
        )
        self.check_for_row_in_list_table(
            '2. Work with Lena and Alan on Sign-and-Read'
        )
        # Natalia wonders whether the site will remember her list. Then she
        # sees that the site has generated a unique URL for her -- there is
        # some explanatory text to that effect.
        self.fail('Finish the test!')

        # She visits that URL - her to-do list is still there.

        # Satisfied, she takes a trip to Cadejo with Eduardo.
