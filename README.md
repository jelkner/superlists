Python TDD Book Version 4
=========================

My fourth journey through Harry J.W. Percival's wonderful
[Test-Driven Development with Python](https://www.obeythetestinggoat.com/).

I'm a high school / community college computer science teacher who teaches
Python programming and web development at the Arlington Career Center in
Arlington, Virginia. 

I'm going through the book again, making more changes to the process,
including using pipenv for virtual environment, the current Django LTS version
(2.2), and writing my own CSS instead of using Bootstrap. to align it with my
learning objectives.

I'm slow, but stubborn, so I keep at it despite the glacial pace with which I
am (or am not) achieving mastery.
